<?php
/**
 * The base configurations of the WordPress.
 *
 * This file has the following configurations: MySQL settings, Table Prefix,
 * Secret Keys, and ABSPATH. You can find more information by visiting
 * {@link http://codex.wordpress.org/Editing_wp-config.php Editing wp-config.php}
 * Codex page. You can get the MySQL settings from your web host.
 *
 * This file is used by the wp-config.php creation script during the
 * installation. You don't have to use the web site, you can just copy this file
 * to "wp-config.php" and fill in the values.
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define('DB_NAME', 'designmodern_theme');

/** MySQL database username */
define('DB_USER', 'designmodern_the');

/** MySQL database password */
define('DB_PASSWORD', 'gieroj1pl');

/** MySQL hostname */
define('DB_HOST', 'localhost');

/** Database Charset to use in creating database tables. */
define('DB_CHARSET', 'utf8');

/** The Database Collate type. Don't change this if in doubt. */
define('DB_COLLATE', '');

/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define('AUTH_KEY',         'f5<z+|o8Yr3Z6a{S&O1X<g$~#d=!yas`zj6W5-1SE~LQ<DJ_0o76hd/|tb=txl+M');
define('SECURE_AUTH_KEY',  'hWV?z]{x}k+uj|lO){7LM7X J=Xf5z#s%^.b{E!S[5@E6VioD._iq=BpBHG H|u5');
define('LOGGED_IN_KEY',    'ZVU-(O{LnEaf+V*5?<$C^?U-AU,e<dl.+R!*.#2_+|n1pf(`kX}(Cch;-_+R[$9p');
define('NONCE_KEY',        'Al[6BdrHJ#QX|(Qe@k)l;<D>3Y?7FlASYpjOsOF&T=<nk-H#J%U.OPO*t2/Jkr;O');
define('AUTH_SALT',        'vEMJ7FqP7mxOb3hP<I ^J-=W($H:l4$ 8&#R_Ld>dR$A$I?Oc5GVCWX?[?XO_@B+');
define('SECURE_AUTH_SALT', 'y9RM,1m9^P%=.i!u=k)]OkB|j_`~>yiAA`ZsXH+dHt=%:5/A]x6Afx+aDtyyr70x');
define('LOGGED_IN_SALT',   '$OCiV2GpBfFg#oR%}0)8PX*^6o5m^U|$i2&V%fZQjU|0S[(Pnr#EG-B+b%Y1`j+w');
define('NONCE_SALT',       '|o@=}<`BMg_R#JpTU|00Tf8@`9|p_H[ ~KD1U) O/x,Dz/ &t@Vb69ME4<],Mwqf');

/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each a unique
 * prefix. Only numbers, letters, and underscores please!
 */
$table_prefix  = 'dm_';

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 */
define('WP_DEBUG', false);

/* That's all, stop editing! Happy blogging. */

/** Absolute path to the WordPress directory. */
if ( !defined('ABSPATH') )
	define('ABSPATH', dirname(__FILE__) . '/');

/** Sets up WordPress vars and included files. */
require_once(ABSPATH . 'wp-settings.php');
